"""
This file contains multiple functions, developed during the analysis of the Fermi-LAT data.
Author: Pietro Monti-Guarnieri, University of Trieste (pietro.monti-guarnieri@phd.units.it)
Last update: 27th January 2025
"""

# ==========================================================================================
#                                  PREAMBLE (module load)
# ==========================================================================================
# Import of the Python Standard Library modules required for the analysis
import glob, os, subprocess, sys       # To act on the operative system
import json                            # To read and write JSON files
import matplotlib as mpl               # For plotting
import matplotlib.pyplot as plt        # For plotting
import numpy as np                     # For numerical analysis
import pandas as pd                    # For reading and writing csv files
import warnings                        # To deactivate the warnings
import time                            # For script timing
import copy                            # To copy objects in memory efficiently
import scipy.stats as sp               # To use certain known PDFs and CDFs

# Import of the astropy/gammapy/fermipy (& fermitools) modules necessary for astrophysics
import gt_apps                             # To use the fermitools in Python
import astropy.units as u                  # Celestial units of measurement
import pyLikelihood as pyLike              # Likelihood analysis
from astropy.io import fits                # I/O of .fits files
from GtApp import GtApp                    # fermitools in Python
from astropy.coordinates import SkyCoord   # To work with sky frames
from UnbinnedAnalysis import UnbinnedObs, UnbinnedAnalysis # To use gtlike in python

# Set the warnings of a few functions
np.seterr(divide='ignore', invalid='ignore')                    # To avoid 0/0 errors
warnings.filterwarnings("ignore", category=DeprecationWarning)  # To avoid deprecation warnings

# ===========================================================================================================
#                                         BASIC UTILITIES
# ===========================================================================================================
# Here is an ensemble of basic utilities for several purposes

"""
Inverse gaussian function

This function is the inverse of a standard normalized gaussian function (with null mean and unit variance),
defined only in the positive x domain.

Inputs:
    - "y"               float or 1D-array, the y values of the gaussian function for which
                          the x values should be found.
    
Outputs:
    - "x"               float of 1D-array, the corresponding x values.
"""

def invgaussfun(y):
    return(np.sqrt(2*np.log(1 / (np.sqrt(2*np.pi)*y) ) ) )


''' 
Points-over-threshold calculator function

This function takes as input a pair of vectors, x-y, and a threshold. It then returns a list
of all the intervals in x and y where y is larger than the threshold (e.g., time and signal). 
Sufficiently close intervals are merged before the return (and the user controls the notion of closeness)

Inputs:
    - "x"                       1D-array, the x values (e.g., time)
    - "y"                       1D-array, the y values (e.g., signal)
    - "thr"                     float, the threshold for the comparison of the y vector
    - "mergedist"               int, the maximum distance between adjacent clusters of points-over-thr
                                  which will lead to merging in the processing (default value: 2)

The outputs are all list of lists, each one corresponding to a cluster of points-over-thr.
Outputs: 
    - "merged_clusters_inds"    the indexes of the points in the cluster
    - "merged_clusters_x"       the x values of the points in the cluster
    - "merged_clusters_y"       the y values of the points in the cluster

'''

def tot_intervals(x,y,thr,mergedist=None):
    # ===============================================
    #                   CROSSCHECK
    # ===============================================
    # I use a try/except because if np.shape(x,y) were to be
    # equal to (), the comparison would throw an error and thus
    # still lead to the following if being entered
    try:
        cond = (np.shape(x)[0] <= 1) | (np.shape(y)[0] <= 1)
    except:
        cond = True
    
    if cond:
        merged_clusters_inds = []
        merged_clusters_x = []
        merged_clusters_y = []
        return(merged_clusters_inds,merged_clusters_x,merged_clusters_y)

    # ===============================================
    #                   CREATE
    # ===============================================
    # Iterate over every value of the y set and check if 
    # it is greater than the threshold. If so, append it.
    all_clusters_inds = [ [] ]
    all_clusters_x = [ [] ]
    all_clusters_y = [ [] ]
    for i,(elx,ely) in enumerate(zip(x,y)):
        if (ely >= thr):
            # Point over threshold: append to the
            # last open cluster.
            all_clusters_inds[-1].append(i)
            all_clusters_x[-1].append(elx)
            all_clusters_y[-1].append(ely)
        else:
            # Point under threshold: append a new cluster to the set
            all_clusters_inds.append([])
            all_clusters_x.append([])
            all_clusters_y.append([])
    
    # After the creation of all the clusters, remove
    # the empty ones. There could be one at index 0
    # if the first point of the y vector was under threshold.
    nclusters = len(all_clusters_inds)
    new_clusters_inds = []
    new_clusters_x = []
    new_clusters_y = []
    for i in range(nclusters):
        ind = all_clusters_inds[i]
        x = all_clusters_x[i]
        y = all_clusters_y[i]
        
        if (ind != []) & (x != []) & (y != []):
            new_clusters_inds.append(ind)
            new_clusters_x.append(x)
            new_clusters_y.append(y)
    
    # ===============================================
    #                   CROSS-CHECK
    # ===============================================
    # If there are no clusters remaining, this means
    # that no point was over threshold. Thus, skip
    # the merge and return
    nclusters = len(new_clusters_inds)
    if nclusters < 1:
        print(f"No point over threshold was detected!")
        return([],[],[])


    # ===============================================
    #                   MERGE
    # ===============================================
    # Merge the clusters whose edges are distant less
    # than the user-given parameter
    if mergedist is not None:
        merged_clusters_inds = [ new_clusters_inds[0] ]
        merged_clusters_x = [ new_clusters_x[0] ]
        merged_clusters_y = [ new_clusters_y[0] ]

        for i in range(nclusters-1):
            # Compute the distance between the last element
            # of the previous cluster and the first element
            # of the new cluster. If this is less than the 
            # user-input, the clusters should be merged
            delta = new_clusters_inds[i+1][0] - new_clusters_inds[i][-1]

            if (delta <= mergedist):
                merged_clusters_inds[-1] += new_clusters_inds[i+1]
                merged_clusters_x[-1] += new_clusters_x[i+1]
                merged_clusters_y[-1] += new_clusters_y[i+1]
            else:
                merged_clusters_inds.append(new_clusters_inds[i+1])
                merged_clusters_x.append(new_clusters_x[i+1])
                merged_clusters_y.append(new_clusters_y[i+1])
    else:
        merged_clusters_inds = new_clusters_inds
        merged_clusters_x = new_clusters_x
        merged_clusters_y = new_clusters_y
   
    return(merged_clusters_inds,merged_clusters_x,merged_clusters_y)


"""
Circular ROI integrator

This function takes as input a 2D-array and returns the sum of all its values, contained in a
circular region with given radius and center equal to the center of the array itself.

Inputs:
    - "data"            1D-array, the data to be summed
    - "radius"          float, the radius of the circular area to be summed, expressed
                          in pixel units
    
Outputs:
    - "datasum"         float, the sum of the requested ROI
"""

def SumCircleFromArray(data,radius):
    # Funzione per sommare tutti gli elementi in "data"
    # che stanno a distanza pari o inferiore a "radius"
    # dal centro.
    nrows, ncols = np.shape(data) # must be 2D
    i0 = round((nrows-1)/2)
    j0 = round((ncols-1)/2)
    datasum = 0
    for i in range(nrows):
        for j in range(ncols):
            dist_ij = np.sqrt((i-i0)**2+(j-j0)**2)
            if (dist_ij <= radius*np.sqrt(2)):
                datasum += data[i,j]
    return(datasum)

"""
gttsmap getter function

This function takes as input the output of the gttsmap command and the informations about
the space structure (how many pixels were analyzed in the map and with which dimension)
and returns the map in a ready-to-use format (where ready-to-use means that it is compatible
with histoplotter2D)

Inputs:
    - "datafile"        str, path of the gttsmap output file
    - "ROI_ra"          float, x center of the gttsmap (as given in the gttsmap "xref" argument)
    - "ROI_dec"         float, y center of the gttsmap (as given in the gttsmap "yref" argument)
    - "nxpix"           float, number of pixel along x (as given in the gttsmap "nxpix" argument)
    - "nypix"           float, number of pixel along y (as given in the gttsmap "nypix" argument)
    - "binsz"           float, pixel width (as given in the gttsmap "binsz" argument)
    - "ifzero"          bool, whether to floor at zero any negative value (default: True)
    
Outputs:
    - "tsmap"           2D-array, the map of the TS values
    - "binsx"           1D-array, the x bins of the map
    - "binsy"           1D-array, the y bins of the map
"""

def getgttsmap(datafile,ROI_ra,ROI_dec,nxpix,nypix,binsz,ifzero=True):
    # Apro il file ed estraggo la tsmap
    with fits.open(datafile) as file:
        rawtsmap = file[0].data

    # la converto in array
    tsmap = np.array(rawtsmap.data).transpose()
    
    # Creo il binning
    binsx = np.arange(ROI_ra   - (nxpix/2)*binsz, ROI_ra  + (nxpix/2 + 1)*binsz, binsz)
    binsy = np.arange(ROI_dec  - (nypix/2)*binsz, ROI_dec + (nypix/2 + 1)*binsz, binsz)
    binsx = binsx[::-1]

    # Se necessario, azzero i pixel negativi
    if ifzero:
        tsmap[tsmap < 0] = 0
    return(tsmap,binsx,binsy)


"""
Function for the estimation of a source emission rate

This function takes as main input a list of timestamps, corresponding to the detection
of photons with the Fermi LAT (within a certain energy band). Then, the list is analyzed
to determine a sort of "source emission rate" as a function of the time. This rate should
reflect the variation of emission intensity over time, i.e., the presence of short bursts
with a rate much higher than the mission-long background average. 

There are multiple ways to compute the emission rates. The method here implemented 
is based on the Finite Difference between the emission times vector. In other words,
the rate is estimated by taking the difference between the emission times vector and its
translation in time by some steps (then the difference is inverted and properly normalized).

Inputs:
    - "data_times"      1D-array, timestamps of the photon in the chosen energy band,
                          expressed in MET and extracted from the Fermi photon files
   - "FDnum"           int, the translation factor in the FD algorithm, meaning that
                          the computed difference is such as vec[FDnum:] - vec[:-FDnum]
                          (default value: 3)

The outputs are organized in a dictionary: "outputdict". The keys are:
    - "out_times"           1D-array, the times associated to each FD-estimated rate.
    - "out_rates"           1D-array, the corresponding source emission rates.
    - "av_rate"             1D-array, the average of the out_rates vector
    - "significance_geom"   1D-array, the significance of each emission rate w.r.t. the
                              mission-long average. This is a geometric significance, i.e.,
                              the ratio of the emission rate and the average rate.
"""

def nu_estimator(data_times,FDnum=3):
    # Compute the times and differential rates
    out_times = data_times[FDnum:]
    out_rates = (FDnum-1) / (data_times[FDnum:] - data_times[:-FDnum])

    # Compute the average rate and derive the significance
    av_rate = np.mean(out_rates)
    significance_geom = out_rates / av_rate

    # Return everything
    outputdict = {"out_times":         out_times,
                  "out_rates":         out_rates,
                  "av_rate":           av_rate,
                  "significance_geom": significance_geom}
    return(outputdict)

'''
Functions for pacman clustering of a TS map

This is a pair of functions, one of which calls the other. The aim of this pair
is to perform a pacman-style segmentation of a binary 2D-array, i.e., a matrix
composed only of 1s and 0s. The idea is to analyze the array and determine the
clusters composing the image, where a cluster is simply defined as a set of
pixel with a value of 1 and without loss of continuity (i.e., each pixel is 
adjacent to at least one of the others). 

The segmentation is performed with a recursive function, which iterates over
every non-zero pixel of the matrix and starts a search of contiguous triggered
pixel from there. Specifically, the "master" function called by the user is
tsmapclustering, which works in this way:
- It generates a "to do list": list of points to be classified in clusters
- For each non null point in the matrix, it checks if the point is on the list.
    If so, it starts a search of the contiguous points in the cluster. This is
    done calling the "slave" functon called clusterfinder. This function returns
    a list of all the points contiguous to the under analysis and belonging to
    the same cluster. Such points are listed as done in the continuously-updated
    to-do list. Note that the to-do list is NOT the list on which this function
    is iterating!
- At the end, the function computes the centroid of each cluster and its radius,
    where the former is the average point in x and y, and the latter is the maximum
    euclidean distance between the centroid and any pixel in the cluster
- Depending on the user, the output can be given as index of the initial matrix
    or in the physical units of the matrix itself. In the latter case, the user
    should provide the original binning vectors for the map, as used in 
    histoplotter2D() or as given by getgttsmap().

The clusterfinder function works in this way:
- It starts a search from a given pixel
- It looks to the closest neighbors of the pixel and checks if they are in the
    to-do list. If so, the points are removed from the latter and added to the 
    cluster list
- The function iterates over every remaining point in the cluster list, and for
    each one it calls itself recursively. In this way, the function is exited
    if and only if the cluster is finished.

------------------------------------------------------------------------------------
Inputs of tsmapclustering():
    - "boolmat"             2D-array, the matrix to be clustered, containing only 1s
                              and 0s (the clusters are the 1s).
    - "mode"                str, an argument passed to "clusterfinder" specifying what
                              is actually a cluster. Accepted values: "1NN" to indicate
                              that only continuous streaks of contiguous points are
                              accepted (default value: "1NN")
    - "ifrescaleoutput"     bool, whether to provide the output clusters as sets of
                              indices (False) or as sets of coordinates (True). Note that
                              this also holds for the radiuses and so on. Note that binsx
                              and binsy must not be None, otherwise the scaling is not
                              triggered (default value: False)
    - "binsx"               1D-array or None, the binning vector for the x axis of the
                              matrix, used in the rescaling of the indices in coordinates.
                              (default: None, meaning that no scaling is done)
    - "binsy"               1D-array or None, the binning vector for the x axis of the
                              matrix, used in the rescaling of the indices in coordinates.
                              (default: None, meaning that no scaling is done)

IMPORTANT NOTE: binsx and binsy, if given, should be cast in a particular form. First of all,
binsx, binsy and boolmat must be the same that are passed to MyFunctions.histoplotter2D()
in order to show the map in 2D. This means that the same vector must be given as input to both
functions, otherwise there may be mismatches between the graphical representations. Secondly,
the vector binsx must be decreasing as indexes go from 0 to -1, while binsy must increase
(i.e., be normally ordered). This is weird for the standards of histogram creation, but it
is what works best with the Fermitools. Thus, if one may want to use this function without the
Fermitools but just with a matrix created through MyFunctions.truehisto2D(), then one should:
    - Create the histogram through truehisto2D()
    - Invert the binsx vector: binsx = binsx[::-1]
    - Call tsmapclustering() passing binsx, binsy and boolmat as arguments
    - Call histoplotter2D() passing binsx, binsy and boolmat as arguments


Outputs of tsmapclustering():
    - "clusterCenters"      2D-array, with each row being the center of a different cluster.
                              The columns are simply the x and y of each center.
    - "clusterData"         dict, containing some more details about each cluster. Currently
                              the available keys are: 
                              - "clusterRadius", 1D-array containing an estimate of the extension
                              of each cluster. This is computed simply as the average euclidean
                              distance between the pixels in the cluster and the center.
                              If "ifrescaleoutput" is True and "binsx" and "binsy" are 1D-arrays,
                              then "clusterRadius" will be rescaled too, from indices to coordinates

                              - "clusterPoints", list containing in each element a list of all the 
                              points belonging to a certain cluster. Each point is formatted as a 
                              tuple of (x,y) coordinates. If "ifrescaleoutput" is True and "binsx" 
                              and "binsy" are 1D-arrays, then the points will be rescaled too, from 
                              indices to coordinates.

------------------------------------------------------------------------------------
Inputs of clusterfinder():
    - "thisPoint"           tuple, x and y of the point under examination.
    - "pointsThisCluster"   list of lists, each containing the points belonging to a certain
                              cluster. The points are formatted as (x,y) tuples
    - "pointsToDo"          list of tuples, each refering to a point which must still be
                              analyzed (since it is over threshold AND non associated to any
                              cluster)
    - "mode"                str, the definition of a cluster. Accepted values: "1NN" means
                              that the points belonging to a cluster are all contiguous, i.e.,
                              1st neighbors (default value: "1NN")

Outputs of clusterfinder():
    - "pointsThisCluster"   as the input, since this function is recursive
    - "pointsToDo"          as the input, since this function is recursive
'''
def tsmapclustering(boolmat,mode = '1NN',ifrescaleoutput=False,binsx=None,binsy=None):
    # Get x,y of all the good (non zero) pixels and cast them as list of tuples
    xpix, ypix = np.nonzero(boolmat)
    points = [(x, y) for x,y in zip(xpix,ypix)]

    # Initialize the to-do list and the list of the cluster points
    pointsToDo = copy.deepcopy(points)           # The "to-do list"
    clusterPoints = [ ]                          # The list of the points of each clusters
    
    # Iterate over every point to do and perform the clustering
    for i,thisPoint in enumerate(points):
        # If the point is NOT in the to do-list, it should be skipped!
        if thisPoint in pointsToDo:
            # The point must be done: remove it from the to-do list and then
            # find all of its contiguous neighbors
            pointsToDo.remove(thisPoint)
            pointsThisCluster = [thisPoint] # Initialize new cluster as non empty list
            pointsThisCluster,pointsToDo = clusterfinder(thisPoint,pointsThisCluster,
                                                         pointsToDo,mode)
            clusterPoints.append(pointsThisCluster)

    # At this point, all the clusters have been found. Thus, we iterate on them
    # to compute their centroids and extensions (radius)
    numClusters = len(clusterPoints)
    clusterCenters = np.zeros((numClusters,2))
    clusterRadius = np.zeros((numClusters,))
    
    for i,clu in enumerate(clusterPoints):
        # Extract the cluster as ndarray and compute the centroid as average 
        # (x,y) coordinate pair
        cluarray = np.array(clu)
        clusterCenters[i,0], clusterCenters[i,1] = np.mean(clu,axis=0)

        # Compute the radius of the cluster as the average distance between the
        # cluster points and the centroid
        nullclu = cluarray - np.mean(clu,axis=0) # Centering the cluster
        distances = np.sqrt(nullclu[:,0]**2 + nullclu[:,1]**2) # Dist from points to center
        clusterRadius[i] = np.mean(distances)

    # If requested, rescale the outputs in physical units
    if ifrescaleoutput & (binsx is not None) & (binsy is not None):
        '''
        Brief comment on the conversion from pixel to units.
            y: start from the lower edge of binsy and add a number of binning steps 
                equal to the y index which is being scaled + half step to correct 
                the fact that the pixel indices are "centered" in the pixels themselves
            x: as above but instead of starting from the lower edge, start from the 
                upper edge and go down from there. This is done due to how binsx is 
                constructed: the vector should be inverted (i.e., monotonically 
                decreasing), since it is shaped to work with plt.imshow(), which inverts 
                the axis!
        '''
        # Compute the binning step along the x and y axis. Assume bins with
        # equal dimension for simplicity.
        binstepx = np.abs( (binsx[1] - binsx[0]) )
        binstepy = np.abs( (binsy[1] - binsy[0]) )

        # Rescale the cluster points in a new list
        newclusterPoints = []
        for i,cluster in enumerate(clusterPoints):
            # Append an empty list to the new list, to allocate space for a cluster
            newclusterPoints.append([])
            
            for el in cluster:
                # Perform the conversion
                xscaled = binsx[0] - el[0]*binstepx - binstepx/2
                yscaled = el[1]*binstepy + binsy[0] + binstepy/2
                
                # Append the coordinate to a new list
                pointscaled = (xscaled,yscaled)
                newclusterPoints[-1].append(pointscaled)

        # Replace the old list with a new one
        clusterPoints = newclusterPoints
        # --------------------------------------------------------------------

        # Rescale the cluster centers and radiuses identically
        numclusters = len(clusterCenters)
        newclusterCenters = np.zeros_like(clusterCenters)
        newclusterRadius = np.zeros_like(clusterRadius)

        for i in range(numclusters):
            # Perform the conversion. For now, the radius is scaled with the
            # AVERAGE binning step along x and y. To be more accurate, one
            # should repeat the radius calculation weighting the pixel distances
            # by the correct binning steps. However, binstepx and binstepy
            # will almost always be equal, so this is not a huge issue.
            newclusterCenters[i,0] = binsx[0] - clusterCenters[i,0]*binstepx - binstepx/2
            newclusterCenters[i,1] = clusterCenters[i,1]*binstepy + binsy[0] + binstepy/2
            newclusterRadius[i] = clusterRadius[i]*(binstepx+binstepy)/2
            
        # Replace the old vectors with the new ones
        clusterCenters = newclusterCenters
        clusterRadius = newclusterRadius

    # Build the output dictionary
    clusterData = {"clusterCenters": clusterCenters,
                   "clusterRadius": clusterRadius}        
        
    return(clusterPoints,clusterData)

# -----------------------------------------------------------------------

def clusterfinder(thisPoint,pointsThisCluster,pointsToDo,mode='1NN'):
    # Find the points neighboring to "thisPoint" which should belong to
    # the same cluster. They could be the closest neighbors, or anything else
    if mode == '1NN':
        # Consider only the first 4 closest neighbors. Enumerate them in
        # clock-wise order starting from north (i.e. N, E, S, W)
        x0, y0 = thisPoint
        pointsClosest = [ (x0,y0+1), (x0+1,y0), (x0,y0-1), (x0-1,y0) ]

    # Iterate over the closest neighbors
    for point in pointsClosest:
        # Check only the neighbors which are eligible (i.e., in the to-do
        # list) and skip the others
        if point in pointsToDo:
            # If the point is in the to-do list, remove it from the latter
            # and append it to the cluster list
            pointsThisCluster.append(point)
            pointsToDo.remove(point)

            # Call recursively the clusterfinder function, to check also the
            # first neighbors of the neighbors, until no points are left
            pointsThisCluster,pointsToDo = clusterfinder(point,pointsThisCluster,
                                                         pointsToDo,mode)

    # At this point no points are left to check: return to all the former
    # layer in the recursion chain!
    return(pointsThisCluster,pointsToDo)


"""
J2000 formatter function

This function takes as input a pair of coordinates (in decimal RA/DEC degrees) and uses it
to format a J2000 name.

Inputs:
    ...
    
Outputs:
    ...
"""

def J2000formatter(RA,DEC):
    # Create a SkyCoord object with the given coordinates
    SCobj = SkyCoord(ra=RA*u.degree, dec=DEC*u.degree)

    # Use it to obtain the RA hours and minutes
    hours = SCobj.to_string('hmsdms').split('h')[0]
    minutes = SCobj.to_string('hmsdms').split('h')[1].split('m')[0]

    # Return the obtained hours and minutes and add the sign of the
    # DEC and the DEC value
    return(f"J{hours}{minutes}{'+' if (DEC > 0) else '-'}{np.abs(DEC):04.3f}")

'''
Sky grid builder function

This function is used mainly by the HEPA monitor but it is of general use. Its main aim is to build
a grid of points in the galactic latitude - longitude plane, trying to have equal spacing between
each point. Then, a radius should be assigned to each point: the union of the circular regions
produced by each point should give a complete tassellation of the sky. Theoretically, this problem
is unsolvable in an exact way, but there are many workarounds which work reasonably well. 
In this function we implement the "golden spiral" method explained here:

https://extremelearning.com.au/how-to-evenly-distribute-points-on-a-sphere-more-effectively-than-the-canonical-fibonacci-lattice/

The idea is to build a grid of points in a theta-phi (long-lat) space by using the golden ratio
to determine the point following any given one. In this way, we just need to provide as input 
to the algorithm two parameters: the numeber of points to create and the value of the epsilon
parameter (which is essentially a regularization term). Then, we look at the average distance
from each point to its closest neighbors and determine what radius could allow a good sky
coverage. Experimentally, we have found that with n = 750 and epsilon = -0.5 (which is negative
in order to fill better the region around the poles) all the points are within approximately 8.20°
from the first 6 neighbors. The distance is smaller at the poles. This means that with a radius of 
5° we can assure a full coverage of the sky, without holes.

------------------------------------------------------------------------------------
Inputs:
    - n                     int, how many points to generate (default value: 750)
    - epsilon               float, the regularization term. Positive values pull all the points
                              towards the galactic equator, while negative values shrink the points
                              towards the galactic poles (default value: -0.5)
    - radius_deg            float, the radius of all the regions, in deg (default value: 5°)
    
Outputs:
    - grid_ra_deg           1D-array, the RA of the points in the grid, expressed in deg
    - grid_dec_deg          1D-array, the DEC of the points in the grid, expressed in deg
    - grid_blong_deg        1D-array, the galactic longitude of the points in the grid, expressed in deg
    - grid_blat_deg         1D-array, the galactic latitude of the points in the grid, expressed in deg
    - grid_radius_deg       1D-array, the radius associated to the circular region built around each
                              point in the grid, expressed in deg
'''

def skygridbuilder(n=750,epsilon=-0.5,radius_deg=5):
    # Build the grid of galactic latitudes and longitudes using the "golden spiral" method
    goldenRatio = (1 + 5**0.5)/2
    i = np.arange(0, n) 
    grid_blong_rad = 2 *np.pi * i / goldenRatio
    arglat = 1 - 2*(i+epsilon)/(n-1+2*epsilon)
    arglat[arglat <= -1] = -1  # Note that |arglat| is around 1.00001 for i = 0, n-1: we need to floor
    arglat[arglat >= 1] = 1    # any exceeding value, to avoid np.nan outputs from np.arccos
    grid_blat_rad = np.arccos(arglat) - np.pi/2  # Note the domain shifting from [0,pi] to [-pi/2,pi/2]
    
    # Note that blong_rad, as it is defined, is not bounded: it scales linearly with i (which ranges
    # from 0 to n). Thus, we want to remove the periodicity and reduce the domain to [0,2pi] before 
    # converting in degrees.
    grid_blong_rad = grid_blong_rad % (2*np.pi)
    
    # Now we can scale the latitude and longitude to degrees.
    grid_blong_deg = np.degrees(grid_blong_rad)
    grid_blat_deg = np.degrees(grid_blat_rad)
    
    # Convert the latitudes and longitudes from galactic coordinates to RA/DEC (in deg)
    cord = SkyCoord(grid_blong_deg,grid_blat_deg, frame='galactic', unit='deg')
    grid_ra_deg, grid_dec_deg = cord.icrs.ra.deg, cord.icrs.dec.deg

    # Compute the ROI radiuses
    grid_radius_deg = grid_blat_deg*0  + radius_deg
    
    # Return the outputs
    return(grid_ra_deg, grid_dec_deg, grid_blong_deg, grid_blat_deg, grid_radius_deg)


'''
TS to Significance converter

This function converts a Test Statistics value (defined as the log likelihood ratio,
i.e., the standard for the Fermi LAT collaboration) into a significance, expressed in
units of standard deviations. The convertion exploits the fact that, due to Wilks theorem,
the TS follows a chi2 distribution with a number of degrees of freedom given by the
number of parameters left free to vary in a likelihood analysis. This means that
the integral of the chi2 distribution from TS to infinity gives the probability that
the observation of such a dataset is just a statistical fluctuation and no source is
actually present. The integral can be easily converted in a significance, knowing
the shape of the gaussian function and its cdf/sf.

Inputs:
    - TS                    float or 1d-array, the Test Statistics value(s)
    - NumFreePars           float or 1d-array, the number of degrees of freedom in the fit
                              (default value: 1)

Outputs:
    - sigma                 float or 1d-array, the significance of the observation
'''
def TS2sigma(TS,NumFreePars=1):
    # Compute the probability of seeing a fluctuation such as the one given by the TS,
    # considering the chi2 distribution with the input-given number of degrees of freedom
    fluctuationProb = sp.chi2(df=NumFreePars).sf(TS)

    # Convert in units of sigmas with the inverse gaussian function
    sigma = sp.norm.isf(fluctuationProb/2)
    return(sigma)


'''
3FHL/4FGL catalog parser (with subregion selector)

This function takes as input a ROI (with a center given in RA/DEC and a fixed radius)
and returns a list of all the 3FHL sources lying inside the region. It can also return
the entire catalog, if needed. The returned names can be either the original ones,
or the associated ones, depending on which catalog is provided as input

Inputs:
    - catalog_file              [path or str] Where to find the 4FGL/3FHL catalog file (.reg) such as:
                                  gll_psch_v12.reg
                                  gll_psc_v32.reg
                                  gll_psch_v12_assoc.reg
                                  gll_psc_v35_assoc.reg
    - ROI_ra                    [float, deg] Center of the ROI (RA) (default value: 0)
    - ROI_dec                   [float, deg] Center of the ROI (DEC) (default value: 0)
    - ROI_rad                   [float, deg] Radius of the ROI (default value: 10)
    - ifWholeCatalog            [bool] If the entire catalog should be returned, instead of a
                                  subselection. This option overrides the ROI cut
    - fManageExtended           [float] How to manage extended sources:
                                  - 0: include both point and extended sources
                                  - 1: include only point sources
                                  - 2: include only extended sources
    - verbosity                 [int] Verbosity of the output. 0 = no printouts, 1 = INFO printouts

Outputs:
    - good_sources_names        [list of str] names of the sources in the ROI
    - good_sources_coords       [2d-array with dimension 2xN] coordinates of the sources in the ROI, in RA/DEC deg
'''
def regExtractor(catalog_file, ROI_ra = 0, ROI_dec = 0, ROI_rad = 10,
                 ifWholeCatalog = False, fManageExtended = 0, verbosity = 1):
    
    # ---------------------------------------------------------------------------------------------
    if (verbosity >= 1):
        print(f"\nParsing the {catalog_file} file")

    # Open the catalog .reg file and read its content
    with open(catalog_file,'r',encoding="utf-8") as ct_file:
        ct_lines = ct_file.readlines()

    ct_lines = ct_lines[1:] # Cut header
    nlines = len(ct_lines)

    # ---------------------------------------------------------------------------------------------
    # Parse the catalog file lines and fill some lists with the sources RA, DEC and names
    if (verbosity >= 1):
        print(f"Parsing the catalog file...")
    printCheck = 500
    ct_sources_RA = []
    ct_sources_DEC = []
    ct_sources_names = []

    for i in range(nlines):
        # Printouts
        if (i % printCheck == 0) & (verbosity >= 1):
            print(f"Analyzing source n. {i} / {nlines}...")

        # Parse from the catalog file the source data. Remember that .reg files have this syntax
        # (which I report here encapsuled between two "|" symbols):
        # |  fk5;point(0.3107,-7.8075) #  point=cross text={3FHL_J0001.2-0748} |
        # Point sources have only 2 parameters, while extended have 5
        source_pars = np.array(ct_lines[i].split('(')[1].split(')')[0].split(','),dtype=np.float64)
        
        # Determine whether to proceed or not, depending on the user inputs:
        #   - If fManageExtended is 0, we always extract the source
        #   - If fManageExtended is 1, we skip the extended sources
        #   - If fManageExtended is 2, we skip the point sources
        isExtendedSource = (np.shape(source_pars)[0] > 2)
        isPointSource = (isExtendedSource == False)
        ifskip = ( (fManageExtended == 1) & (isExtendedSource) ) | ( (fManageExtended == 2) & (isPointSource) )
        if ifskip:
            continue

        # Parse from the catalog file the RA/DEC center of the source        
        ct_sources_RA.append(source_pars[0])
        ct_sources_DEC.append(source_pars[1])

        # Parse the name
        sourceName = ct_lines[i].split('{')[1].split('}')[0]
        sourceName = sourceName.rstrip().lstrip()
        ct_sources_names.append(sourceName)

    # Convert the lists in string arrays
    ct_sources_RA = np.array(ct_sources_RA, dtype = '<U32')
    ct_sources_DEC = np.array(ct_sources_DEC, dtype = '<U32')
    ct_sources_names = np.array(ct_sources_names, dtype = '<U32')

    # Sort the lists by acending RA
    sortinds = np.lexsort((ct_sources_DEC,ct_sources_RA))
    ct_sources_RA = ct_sources_RA[sortinds]
    ct_sources_DEC = ct_sources_DEC[sortinds]
    ct_sources_names = ct_sources_names[sortinds]

    if (verbosity >= 1):
        print(f"Parsing completed!\n")

    # ---------------------------------------------------------------------------------------------
    # Check if we need to return the whole catalog or not
    if ifWholeCatalog:
        # We want the whole catalog: do nothing
        good_sources_names = ct_sources_names
        good_sources_coords = [ct_sources_RA,ct_sources_DEC]
    else:
        # Iterate over every line in both files and select only the sources in the ROI
        ROICoord = SkyCoord(ROI_ra,ROI_dec,frame='icrs',unit='deg')
        good_sources_names = []
        good_sources_coords = []

        if (verbosity >= 1):
            print(f"Extracting the sources in the ROI")
            print(f"ROI center = ({ROI_ra:.2f}°, {ROI_dec:.2f}°) (RA, DEC)")
            print(f"ROI radius = {ROI_rad:.2f}°")

        for i in range(nlines):
            # Printouts and checksums
            if (i % printCheck == 0) & (verbosity >= 1):
                print(f"Analyzing source n. {i} / {nlines}...")

            # Compute the angular separation between the i-th source and the center of the ROI
            sourceCoord = SkyCoord(ct_sources_RA[i],ct_sources_DEC[i],frame='icrs',unit='deg')
            angSep = ROICoord.separation(sourceCoord)

            # If the separation is smaller than the ROI radius, save the source name and its position
            if angSep.deg <= ROI_rad:
                good_sources_coords.append([ct_sources_RA[i],ct_sources_DEC[i]])
                good_sources_names.append(ct_sources_names[i])
            
        if (verbosity >= 1):
            print(f"Extraction completed!\n")
        
    # Return to main
    good_sources_coords = np.array(good_sources_coords,dtype=np.float64)
    return(good_sources_names,good_sources_coords)

'''
Galactic to equatorial converters

This set of two functions can be used to convert a point from RA/DEC to galactic coordinates,
or vice versa. It also works with arrays.

Inputs (for gal2radec):
    - blong                     [float or 1d-array] The point coordinate in galactic longitude (deg)
    - blat                      [float or 1d-array] The point coordinate in galactic latitude (deg)

Outputs:
    - ra                        [float or 1d-array] The point coordinate in RA
    - dec                       [float or 1d-array] The point coordinate in DEC

For radel2gal the inputs and outputs are the same as above, but inverted
'''

def gal2radec(blong, blat):
    # Build SkyCoord object for the given grid
    cord = SkyCoord(blong,blat, frame='galactic', unit='deg')

    # Compute output
    ra, dec = cord.icrs.ra.deg, cord.icrs.dec.deg

    return(ra, dec)

def radec2gal(ra, dec):
    # Build SkyCoord object for the given grid
    cord = SkyCoord(ra,dec, frame='icrs', unit='deg')

    # Compute output
    blong, blat = cord.galactic.l.deg, cord.galactic.b.deg

    return(blong, blat)

'''
Angular distance calculator

This function computes the angular distance on the great sphere between two points of given coordinates.
All coordinates and angles are here assumed to be given in RADIANS. It should be mentioned that this 
functions works both in galactic coordinates (where lat = b, long = l) and in icrs (where ra = longitude, dec = latitude)

Inputs (for gal2radec):
    - latA                     [float or 1d-array] The latitude of the first point (or set of points)
    - longA                    [float or 1d-array] The longitude of the first point (or set of points)
    - latB                     [float or 1d-array] The latitude of the second point (or set of points)
    - longB                    [float or 1d-array] The longitude of the second point (or set of points)
    - ifdeg                    [bool] If the data are provided as deg. If true: input are scaled to radians before
                                 calculations and the output is rescaled back to degrees. Otherwise, radians are used.

Outputs:
    - angle                    [float or 1d-array] The angular distance between A and B

For radel2gal the inputs and outputs are the same as above, but inverted
'''
def angdist(latA,longA,latB,longB,ifdeg=False):
    # Casting, if required
    latA = np.array(latA)
    longA = np.array(longA)
    latB = np.array(latB)
    longB = np.array(longB)
    if ifdeg:
        latA = np.radians(latA)
        longA = np.radians(longA)
        latB = np.radians(latB)
        longB = np.radians(longB)

    # Checksum for the following reason: long is defined in [0,2pi], lat in [-pi/2,pi/2], angles are assumed to be in radians
    if ifdeg:
        this_pi = 180
    else:
        this_pi = np.pi

    long_min_thr = 0 
    long_max_thr = 2*this_pi
    lat_min_thr = -this_pi/2
    lat_max_thr = +this_pi/2
    if (np.sum(latA < lat_min_thr) + np.sum(latA > lat_max_thr) > 0):
        print(f"Input variable latA has out of bound elements, for given bounds: ({long_min_thr:.2f} to {long_max_thr:.2f})")
    if (np.sum(longA < long_min_thr) + np.sum(longA > long_max_thr) > 0):
        print(f"Input variable longA has out of bound elements, for given bounds: ({long_min_thr:.2f} to {long_max_thr:.2f})")
    if (np.sum(latB < lat_min_thr) + np.sum(latB > lat_max_thr) > 0):
        print(f"Input variable latB has out of bound elements, for given bounds: ({long_min_thr:.2f} to {long_max_thr:.2f})")
    if (np.sum(longB < long_min_thr) + np.sum(longB > long_max_thr) > 0):
        print(f"Input variable longB has out of bound elements, for given bounds: ({long_min_thr:.2f} to {long_max_thr:.2f})")

    # Finally, compute the angle
    angle = np.arccos( np.sin(latA)*np.sin(latB) + np.cos(latA)*np.cos(latB)*np.cos(longA-longB) )

    # If necessary, scale back the value to degrees
    if ifdeg:
        angle = np.degrees(angle)
    return(angle)

'''Circle creator
This functions is used to compute a set of points which are equidistant from a given center,
in the 2D celestial sphere. 

The functions works on the assumption that, given two points (b0,l0), (b1,l1), their angular 
separation is an angle theta that follows:
       cos(theta) = sin(b0) sin(b1) + cos(b0) cos(b1) cos(l0-l1)
This formula can be inverted to find the longitude of a point, given its angular distance 
from another point, and given its latitude. In our case, latitudes fill homogeneously the 
space from south to north, and longitudes are computed in two semi-circles (east and west),
since the inversion of the above formula has a restricted domain.

Inputs:
...

Outputs:
...
'''
def circlecalculator(lat_C,long_C,theta,ifdeg=True,npts=1000):
    # Scale to radians, if necessary
    if ifdeg:
        lat_C = np.radians(lat_C)
        long_C = np.radians(long_C)
        theta = np.radians(theta)

    # Find the points at the same longitude of the center, with the maximum and minimum
    # latitude. Note that if l0 = l1, for the duplication formula, b0 +/- b1 = theta.
    lat_low = lat_C - theta
    lat_up = lat_C + theta
    lat_vec = np.linspace(np.max([lat_low,-np.pi/2]),np.min([lat_up,np.pi/2]),npts)

    # Apply the inverted formula of the one given above to compute the longitude
    angterm = np.arccos( ( np.cos(theta) - np.sin(lat_C) * np.sin(lat_vec) ) / ( np.cos(lat_C) * np.cos(lat_vec) ) )
    long_vec_plus = long_C + angterm
    long_vec_minus = long_C - angterm

    # Fix the extremes at the same longitude of the center
    long_vec_plus[0] = long_vec_minus[0] = long_C
    long_vec_plus[-1] = long_vec_minus[-1] = long_C

    # Round-off any point outside boundaries
    long_vec_minus[long_vec_minus <= 0] = 0
    long_vec_minus[long_vec_minus >= 2*np.pi] = 2*np.pi
    long_vec_plus[long_vec_plus <= 0] = 0
    long_vec_plus[long_vec_plus >= 2*np.pi] = 2*np.pi

    # It is possible that "angterm" has some NaNs, if the argument of the arccos function
    # is not in [-1,1]. Physically, this means that any longitude is acceptable, since the
    # corresponding distance is < theta. Thus, we just flat out the arrays to the minimum
    # and maximum values available.
    long_vec_minus[np.isnan(long_vec_minus)] = 0
    long_vec_plus[np.isnan(long_vec_plus)] = 2*np.pi

    if ifdeg:
        lat_vec = np.degrees(lat_vec)
        long_vec_plus = np.degrees(long_vec_plus)
        long_vec_minus = np.degrees(long_vec_minus)

    return(lat_vec, long_vec_plus, long_vec_minus)

'''Duplicate remover
This function is used when a list of sources on the celestial sphere is provided, and we want to remove
possible "duplicates" from the list. Here, duplicate means "two sources too close to be actually
distinct"

Inputs:
- sources_lat          [1d-array] Latitude of the sources (galactic latitude or DEC)
- source_long          [1d-array] Longitude of the sources (galactic longitude or RA)
- minAngDist           [float] Maximum angular separation between two sources
- ifdeg                [bool] Are the coordinates and minAngDist given in degree (True) or radians (False)?
- verbosity            [int] Verbosity for the call of this function (0 = no verbosity)

Outputs:
- indList              [list] The list of indices of the sources to be kept
'''
def duplicateRemoval(sources_lat,sources_long,minAngDist=0.2,ifdeg=True,verbosity=1):
    # Iterate over every source
    numSources = np.shape(sources_lat)[0]
    indList = []
    printCheck = 100

    if (verbosity > 0):
        print(f"")
        print(f"Removing duplicates from list...")
    for i in range(numSources):
        # Check printout
        if ((i % printCheck) == 0) & (verbosity > 0):
            print(f"Step {i} / {numSources}")

        # Compute the distance from the i-th sources to the remaining ones. Note: the i-th
        # seed must only be compared with the next seeds in the list, since the previous have 
        # already been checked. We want also to avoid computing the distance between a seed and itself.
        # Note that this means that the last element in the row actually must always be added, since
        # there's nothing to compare it against 
        if (i < numSources-1):
            distances = angdist(sources_lat[i],sources_long[i],sources_lat[i+1:],sources_long[i+1:],ifdeg=True)
            if (np.nanmin(distances) <= minAngDist):
                continue

        indList.append(i)

    if (verbosity > 0):
        print(f"Done!")
    
    # Cycle ended, return values
    return(indList)






















# ===========================================================================================================
#                                              FERMITOOLS
# ===========================================================================================================
# Here is an ensemble of wrappers built around the fermitools and fermipy to facilitate their use in Python 

"""
gtselect function

This function performs a call to the Fermitool "gtselect" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtselect.txt
    
This function has no outputs.
"""
def gtselect(infile,outfile,ROI_ra=0,ROI_dec=0,ROI_rad=2.5,Emin=100,Emax=300000,zmax=90,
             tstart="INDEF",tstop="INDEF",evclass=128,evtype=3,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtselect'])

    # Set the parameters for the gtselect app
    gt_apps.filter['infile']  = infile    # Input file
    gt_apps.filter['outfile'] = outfile   # Output file
    gt_apps.filter['ra']      = ROI_ra    # [deg] center of the ROI in RA coordinates
    gt_apps.filter['dec']     = ROI_dec   # [deg] center of the ROI in DEC coordinates
    gt_apps.filter['rad']     = ROI_rad   # [deg] radius of the ROI
    gt_apps.filter['zmax']    = zmax      # [deg] maximum zenith angle
    gt_apps.filter['emin']    = Emin      # [MeV] minimum photon energy
    gt_apps.filter['emax']    = Emax      # [MeV] minimum photon energy
    gt_apps.filter['tmin']    = tstart    # [MET] beginning of the time interval
    gt_apps.filter['tmax']    = tstop     # [MET] end of the time interval
    gt_apps.filter['evclass'] = evclass   # Class of the event
    gt_apps.filter['evtype']  = evtype    # Type of the event
    gt_apps.filter['chatter'] = chatter   # Verbosity
    gt_apps.filter['mode']    = gtmode    # ['ql','h'] Mode of execution
    
    # Call the application
    print(f"Calling gtselect... \n")
    gt_apps.filter.run()
    print(f"\nDone!\n")
    return

"""
gtmktime function

This function performs a call to the Fermitool "gtmktime" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtmktime.txt
    
This function has no outputs.
"""

def gtmktime(infile,outfile,SCfile,ROI_ra=None,ROI_dec=None,roicut='no',SUNsep=None,
             filtercond='(IN_SAA!=T)&&(DATA_QUAL>0)&&(LAT_CONFIG==1)',chatter=4,gtmode="h"):
    # Add the sun separation angle (if it is defined) to the filter condition
    if (SUNsep is not None) & (ROI_ra is not None) & (ROI_dec is not None):
        if float(SUNsep) > 0:
            # Assume that we want to be FAR from the Sun
            filtercond += f"&&(angsep({ROI_ra},{ROI_dec},RA_SUN,DEC_SUN)>{np.abs(SUNsep)})"    
        else:
            # Assume that we want to be CLOSE to the Sun
            filtercond += f"&&(angsep({ROI_ra},{ROI_dec},RA_SUN,DEC_SUN)<{np.abs(SUNsep)})"    

    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtmktime'])

    # Set the parameters for the gtmktime app
    gt_apps.maketime['evfile']  = infile        # Input file (from gtselect)
    gt_apps.maketime['outfile'] = outfile       # Output file
    gt_apps.maketime['scfile']  = SCfile        # Spacecraft file
    gt_apps.maketime['filter']  = filtercond    # [str] Filter condition
    gt_apps.maketime['roicut']  = roicut        # Apply ROI correction?
    gt_apps.maketime['chatter'] = chatter       # Verbosity
    gt_apps.maketime['mode']    = gtmode        # ['ql','h'] Mode of execution
    
    # Call the application
    print(f"Calling gtmktime... \n")
    gt_apps.maketime.run()
    print(f"\nDone!\n")
    return

"""
gtbindef function

This function performs a call to the Fermitool "gtbindef" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtbindef.txt
    
This function has no outputs.
"""

def gtbindef(binfile,outfile,bintype='T',chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtbindef'])

    # Set the parameters for the gtbindef app
    bindef = GtApp('gtbindef','Likelihood')
    bindef['binfile'] = binfile     # Binning file
    bindef['outfile'] = outfile     # Output file
    bindef['bintype'] = bintype     # Type of binning
    bindef['chatter'] = chatter     # Verbosity
    bindef['mode']    = gtmode      # ['ql','h'] Mode of execution
    
    # Call the application
    print(f"Calling gtbindef... \n")
    bindef.run()
    print(f"\nDone!\n")
    return

"""
gtbin function

This function performs a call to the Fermitool "gtbin" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtbin.txt
    
This function has no outputs.
"""

def gtbin(infile,outfile,SCfile,tbinfile=None,tstart=0,tstop=100,algorithm='LC',tbinalg='FILE',
          chatter=4,gtmode="h",dtime=1):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtbin'])

    # Set the parameters for the gtbin app. Note that tstart and tstop are ignored if tbinfile
    # is specified.
    gt_apps.evtbin['evfile']    = infile       # Input file (from gtmktime)
    gt_apps.evtbin['outfile']   = outfile      # Output file
    gt_apps.evtbin['scfile']    = SCfile       # Spacecraft file
    gt_apps.evtbin['tstart']    = tstart       # Start of the timing vector
    gt_apps.evtbin['tstop']     = tstop        # Stop of the timing vector
    gt_apps.evtbin['dtime']     = dtime        # Binning step in the time domain (if tbinalg is "LIN")
    gt_apps.evtbin['algorithm'] = algorithm    # Type of output (e.g., Light Curves, MAPs, etc.)
    gt_apps.evtbin['tbinalg']   = tbinalg      # How the time bins will be specified
    gt_apps.evtbin['chatter']   = chatter      # Verbosity
    gt_apps.evtbin['mode']      = gtmode       # ['ql','h'] Mode of execution

    if tbinfile is not None:
        gt_apps.evtbin['tbinfile']  = tbinfile     # Binning file
    
    # Call the application
    print(f"Calling gtbin... \n")
    gt_apps.evtbin.run()
    print(f"\nDone!\n")
    return

"""
gtbin (in CMAP mode) function

This function performs a call to the Fermitool "gtbin" utility, passing to it several 
arguments, setting the CMAP mode of operation.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtbin.txt
    
This function has no outputs.
"""

def gtbin_CMAP(infile,outfile,SCfile,algorithm='CMAP',nxpix=10,nypix=10,
               binsz=0.5,coordsys='GAL',xref=0,yref=0,axisrot=0,
               proj='AIT',chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtbin'])

    # Set the parameters for the gtbin app.
    gt_apps.evtbin['evfile']    = infile       # Input file (from gtmktime)
    gt_apps.evtbin['outfile']   = outfile      # Output file
    gt_apps.evtbin['scfile']    = SCfile       # Spacecraft file
    gt_apps.evtbin['algorithm'] = algorithm    # Force to "CMAP" to have counting map
    gt_apps.evtbin['nxpix']     = nxpix        # Number of pixels from yref in total
    gt_apps.evtbin['nypix']     = nypix        # Number of pixels from yref in total
    gt_apps.evtbin['binsz']     = binsz        # Binning step in x, y
    gt_apps.evtbin['coordsys']  = coordsys     # Coordinate system for xref, yref
    gt_apps.evtbin['xref']      = xref         # x center of the frame
    gt_apps.evtbin['yref']      = yref         # y center of the frame
    gt_apps.evtbin['axisrot']   = axisrot      # Rotation angle in the x-y frame
    gt_apps.evtbin['proj']      = proj         # ['AIT','CART', ...] Projection style
    gt_apps.evtbin['chatter']   = chatter      # Verbosity
    gt_apps.evtbin['mode']      = gtmode       # ['ql','h'] Mode of execution

    # Call the application
    print(f"Calling gtbin in CMAP mode... \n")
    gt_apps.evtbin.run()
    print(f"\nDone!\n")
    return

"""
gtexposure function

This function performs a call to the Fermitool "gtexposure" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtexposure.txt
    
This function has no outputs.
"""

def gtexposure(infile,SCfile,irfs='P8R3_SOURCE_V3',scrmdl="none",specin=-2.1,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtexposure'])

    # Set the parameters for the gtexposure app
    exposure = GtApp('gtexposure','Likelihood')
    exposure['infile']  = infile         # Input file
    exposure['scfile']  = SCfile         # Spacecraft file
    exposure['irfs']    = irfs           # [str] IRFs
    exposure['srcmdl']  = scrmdl         # Source model
    exposure['specin']  = specin         # Photon spectral index to use for weighting the exposure
    exposure['chatter'] = chatter        # Verbosity
    exposure['mode']    = gtmode         # ['ql','h'] Mode of execution

    # Call the application
    print(f"Calling gtexposure... \n")
    exposure.run()
    print(f"\nDone!\n")
    return  


"""
gtltcube function

This function performs a call to the Fermitool "gtltcube" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtltcube.txt
    
This function has no outputs.
"""

def gtltcube(infile,outfile,SCfile,zmax=90,dcostheta=0.025,binsz=0.25,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtltcube'])

    # Set the parameters for the gtltcube app
    gt_apps.expCube['evfile']    = infile
    gt_apps.expCube['outfile']   = outfile
    gt_apps.expCube['scfile']    = SCfile    
    gt_apps.expCube['zmax']      = zmax
    gt_apps.expCube['dcostheta'] = dcostheta
    gt_apps.expCube['binsz']     = binsz
    gt_apps.expCube['chatter']   = chatter
    gt_apps.expCube['mode']      = gtmode

    # Call the application
    print(f"Calling gtltcube... \n")
    gt_apps.expCube.run()
    print(f"\nDone!\n")
    return

"""
gtexpcube2 function

This function performs a call to the Fermitool "gtexpcube2" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtexpcube2.txt
    
This function has no outputs.
"""

def gtexpcube2(infile,outfile,cmap='none',irfs='P8R3_SOURCE_V3',evtype='INDEF',nxpix=1,
               nypix=1,binsz=0.25,coordsys='GAL',xref=0,yref=0,axisrot=0,proj='AIT',
               ebinalg='LOG',Emin=1e3,Emax=2e6,enumbins=20,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtexpcube2'])

    # Set the parameters for the gtexpcube2 app
    gtexpCube2 = GtApp('gtexpcube2','Likelihood')
    gtexpCube2['infile']    = infile
    gtexpCube2['outfile']   = outfile
    gtexpCube2['cmap']      = cmap
    gtexpCube2['irfs']      = irfs
    gtexpCube2['evtype']    = evtype
    gtexpCube2['nxpix']     = nxpix
    gtexpCube2['nypix']     = nypix
    gtexpCube2['binsz']     = binsz
    gtexpCube2['coordsys']  = coordsys
    gtexpCube2['xref']      = xref
    gtexpCube2['yref']      = yref
    gtexpCube2['axisrot']   = axisrot
    gtexpCube2['proj']      = proj
    gtexpCube2['ebinalg']   = ebinalg
    gtexpCube2['emin']      = Emin
    gtexpCube2['emax']      = Emax
    gtexpCube2['enumbins']  = enumbins
    gtexpCube2['chatter']   = chatter
    gtexpCube2['mode']      = gtmode

    # Call the application
    print(f"Calling gtexpcube2... \n")
    gtexpCube2.run()
    print(f"\nDone!\n")
    return

"""
gtexpmap function

This function performs a call to the Fermitool "gtexpmap" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtexpmap.txt
    
This function has no outputs.
"""

def gtexpmap(infile,outfile,SCfile,expcubefile,evtype="INDEF",irfs="CALDB",srcrad=10,nlong=10,
             nlat=10,nenergies=20,chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtexpmap'])

    # Set the parameters for the gtexpmap app
    gt_apps.expMap['evfile']    = infile
    gt_apps.expMap['outfile']   = outfile
    gt_apps.expMap['scfile']    = SCfile
    gt_apps.expMap['expcube']   = expcubefile
    gt_apps.expMap['evtype']    = evtype
    gt_apps.expMap['irfs']      = irfs
    gt_apps.expMap['srcrad']    = srcrad
    gt_apps.expMap['nlong']     = nlong
    gt_apps.expMap['nlat']      = nlat
    gt_apps.expMap['nenergies'] = nenergies
    gt_apps.expMap['chatter']   = chatter
    gt_apps.expMap['mode']      = gtmode

    # Call the application
    print(f"Calling gtexpmap... \n")
    gt_apps.expMap.run()
    print(f"\nDone!\n")
    return

"""
gtdiffrsp function

This function performs a call to the Fermitool "gtdiffrsp" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gtdiffrsp.txt
    
This function has no outputs.
"""

def gtdiffrsp(infile,SCfile,srcmdl,irfs='CALDB',clobber='yes',chatter=4,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gtdiffrsp'])

    # Set the parameters for the gtdiffrsp app
    gt_apps.diffResps['evfile']  = infile
    gt_apps.diffResps['scfile']  = SCfile
    gt_apps.diffResps['srcmdl']  = srcmdl
    gt_apps.diffResps['irfs']    = irfs
    gt_apps.diffResps['clobber'] = clobber
    gt_apps.diffResps['chatter'] = chatter
    gt_apps.diffResps['mode']    = gtmode

    # Call the application
    print(f"Calling gtdiffrsp... \n")
    gt_apps.diffResps.run()
    print(f"\nDone!\n")
    return

"""
gttsmap function

This function performs a call to the Fermitool "gttsmap" utility, passing to it several 
arguments.

The inputs follow the conventions outlined in the official help of the fermitools:
    https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/fhelp_files/gttsmap.txt
    
This function has no outputs.
"""

def gttsmap(infile,outfile,SCfile,expcubefile,expmapfile,srcmdl,statistics="UNBINNED",
            irfs="CALDB",optimizer="NEWMINUIT",nxpix=10,nypix=10,binsz=0.025,coordsys="CEL",
            xref=0,yref=0,proj="AIT",chatter=2,gtmode="h"):
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','gttsmap'])

    # Set the parameters for the gttsmap app
    gt_apps.TsMap['evfile'] = infile
    gt_apps.TsMap['outfile'] = outfile
    gt_apps.TsMap['scfile'] = SCfile
    gt_apps.TsMap['expcube'] = expcubefile
    gt_apps.TsMap['expmap'] = expmapfile
    gt_apps.TsMap['srcmdl'] = srcmdl
    gt_apps.TsMap['statistic'] = statistics
    gt_apps.TsMap['irfs'] = irfs
    gt_apps.TsMap['optimizer'] = optimizer
    gt_apps.TsMap['nxpix'] = nxpix
    gt_apps.TsMap['nypix'] = nypix
    gt_apps.TsMap['binsz'] = binsz
    gt_apps.TsMap['coordsys'] = coordsys
    gt_apps.TsMap['xref'] = xref
    gt_apps.TsMap['yref'] = yref
    gt_apps.TsMap['proj'] = proj
    if chatter <= 2:
        gt_apps.TsMap['chatter'] = chatter
    else:
        # I'm adding an artificial cap to the chatter here, because a value of 4 would
        # absolutely render the terminal screen unreadable. This can be changed, if the user
        # really wants a complete output...
        gt_apps.TsMap['chatter'] = 2
    gt_apps.TsMap['mode'] = gtmode

    # Call the application
    print(f"Calling gttsmap... \n")
    gt_apps.TsMap.run()
    print(f"\nDone!\n")
    return

"""
gtlike function

This function performs a call to the Fermitool "gtlike" utility, passing to it several 
arguments. In this case, we do not simply wrap code around the official utility,
but rather use the Fermipy implementation of the function.

Inputs:
    - infile            [str] the path of the photon input file, as given by gtmktime
    - outfile           [str] the path of the XML output file, which will include all
                          the sources already present in the input XML file + the sources
                          here fitted
    - SCfile            [str] the path of the spacecraft input file
    - expcubefile       [str] the path of the LAT livetime file, as given by gtltcube
    - expmapfile        [str] the path of the LAT livetime file, as given by gtexposure
    - srcmdl            [str] the path of the XML input file, containing a list of all
                          the sources known in the area (+ eventual flares)
    - irfs              [str] the IRFs to be used in the fitting (default value: "CALDB",
                          meaning something like Pass 8 release 3)
    - optimizer         [str] the optimizer to be used in the likelihood fits (defualt value:
                          "NewMinuit")
    - verbosity         [int] the verbosity for the execution (default value: 1, relatively
                          low verbosity for the call)

This function has no outputs.
"""

def gtlike(infile,outfile,SCfile,expcubefile,expmapfile,srcmdl,irfs='CALDB',
           optimizer='NewMinuit',verbosity=1):
    # Set the parameters for the gtlike app wrapped in Python by pyLikelihood
    print(f"Initializing gtlike...")
    obs = UnbinnedObs(infile,SCfile,expCube=expcubefile,expMap=expmapfile,irfs=irfs)
    like = UnbinnedAnalysis(obs,srcmdl,optimizer=optimizer)
    if optimizer == 'NewMinuit':
        likeobj = pyLike.NewMinuit(like.logLike)
    print(f"Initialization completed!\n")

    # Call the application
    print(f"Running gtlike...")
    try:
        like.fit(verbosity=verbosity,covar=True,optObject=likeobj)

        # Save the output in the corresponding file and return the exit code
        like.logLike.writeXml(outfile)
        returncode = likeobj.getRetCode()
    except:
        like = None
        returncode = -999
        
    print(f"gtlike exit code: {returncode} \n")
    return(like)


"""
pgwave2D function

This function performs a call to the Fermitool "pgwave2D" utility, passing to it several 
arguments. Note that this function only works for Fermitools with releases up to 2.0.8,
so a proper conda environment should be used.
    
Since there is no fhelp file, we list here the meaning of the pgwave2D parameters:
    - input_file             Input image file name
    - bgk_choise             Do you have a Background map?
    - input_bgk_file         Path of the background map, or 'none'
    - output_file_prefix     Prefix of the output file name
    - circ_square            Is the image Circular or Squared?
    - N_iterations           Number of scanning iterations
    - SN_ratio               Signal-To-Noise ratios (separated by an empty space)
    - N_scale                Number of wavelet scales
    - scala                  Mexican Hat scales (separated by an empty space) (number of pixels!)
    - otpix                  Minimum number of connected pixels to identify a source
    - n_sigma                Multiplying factor for the Mexican Hat scale equivalent to the sigma of the gaussian filter
    - median_box             Box coefficient for the median filter
    - r_threshold            Radius for the threshold area
    - kappa                  Number of sigmas relative to the statistical confidence
    - min_pix                Minimum pixel distance between two different sources
    - m_num                  Minimum number of connected pixels to identify a source
    - border_size            Number of border pixels
    - s_box                  Segmentation paramenter for the symmetry box to compare with sources:
    - fitsio_choice          Print all the intermediate fits-files?
    - recursive_choice       Enable the consecutive-scale condition option?
    - verbose_level          Print intermediate results

This function has no outputs.
"""

def pgwave2D(infile,bgk_choise='n',bkgfile=None,output_file_prefix=None,circ_square='s',
             N_iterations=1,SN_ratio=0,N_scale=1,scala=2.2,otpix=5,n_sigma=7.0,
             median_box=3.0,r_threshold=0.5,kappa=2.6,min_pix=3,m_num=10,
             border_size=5,s_box=5,fitsio_choice='n',recursive_choice='y',verbosity=0):
    # Check if the environment supports the use of pgwave
    # Clear the parameters from previous selections
    subprocess.run(['punlearn','pgwave2D'])

    # Set the parameters for the pgwave2D app
    pgw = GtApp('pgwave2D')
    pgw['input_file'] = infile
    pgw['bgk_choise'] = bgk_choise
    if ((bgk_choise == 'y') & (bkgfile is not None)):
        pgw['input_bgk_file'] = bkgfile

    if (output_file_prefix is not None):
        pgw['output_file_prefix'] = output_file_prefix

    pgw['circ_square'] = circ_square
    pgw['N_iterations'] = N_iterations 
    pgw['SN_ratio'] = SN_ratio
    pgw['N_scale'] = N_scale
    pgw['scala'] = scala
    pgw['otpix'] = otpix
    pgw['n_sigma'] = n_sigma
    pgw['median_box'] = median_box
    pgw['r_threshold'] = r_threshold
    pgw['kappa'] = kappa
    pgw['min_pix'] = min_pix
    pgw['m_num'] = m_num
    pgw['border_size'] = border_size
    pgw['s_box'] = s_box
    pgw['fitsio_choice'] = fitsio_choice
    pgw['recursive_choice'] = recursive_choice
    pgw['verbose_level'] = verbosity
    
    # Call the application
    print(f"Calling pgwave2D... \n")
    pgw.run()
    print(f"\nDone!\n")
    return