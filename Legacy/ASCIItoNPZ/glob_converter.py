'''
----------------------------------------------------------------------------------------
                                ASCII to NPZ CONVERTER SCRIPT
----------------------------------------------------------------------------------------
The aim of this script is to take a huge ASCII file as input (like the ones produced raw
in a test beam run) and convert it to a NPZ file. This conversion is made entirely in
Python. The idea is the following: there are two support scripts. The first is needed
since the original ASCII file is written by a digitizer, hence it has the typical
structure with 1 long line (e.g., with the PH and times of measurement and event index
and so on) followed by multiple lines (each containing a waveform). The second script
is simply the GZIP to NPZ file converter.
----------------------------------------------------------------------------------------
                                INSTRUCTIONS FOR USAGE
----------------------------------------------------------------------------------------
In order to execute this script, simply put it in a folder (where there should be, also,
the "ascii2gzip_converter.py" file and the "gzip2npz_converter.py" file. Then, adjust
the custom inputs (such as the columns dtypes, or the periodicity of the ASCII lines).
Finally, open a terminal and run the command:

python3 /eos/user/p/pmontigu/Functions/ASCIItoNPZ/glob_converter.py [run number, e.g., 360260]
[ifwave, set to True if you want to have also the waveforms in the npz file]

For example:

python3 /eos/user/p/pmontigu/Functions/ASCIItoNPZ/glob_converter.py 360260 True

----------------------------------------------------------------------------------------
                                INSTRUCTIONS FOR FUTURE EDITS
----------------------------------------------------------------------------------------
In case some detectors are added or removed from the setup, it is important to correctly
edit this script. In particular:
    1. Edit under "First process call" the new periodicity of the ascii files
    2. Edit under "End of first process call" the new names and lengths of the coulmns
    and the total number of variables
    3. Edit the definition of "col_dtypes" and "col_inds"
    4. Edit the new content of "GZIP_outputfile"
'''

# First of all, import the required modules
import os, glob, subprocess, pickle, time, sys
import numpy as np

# Proceed with the script
if __name__ == '__main__':
    # Define the names of the variables referencing to the folders across the script
    locfolder = '/eos/user/p/pmontigu/Functions/ASCIItoNPZ/'
    ASCII_processfile = locfolder + 'ascii2gzip_converter.py'
    GZIP_processfile = locfolder + 'gzip2npz_converter.py'
    
    runfile = 'run' + str(sys.argv[1])
    outfolder = '/eos/user/p/pmontigu/Datadirs/datadir_2022/Insulab/Genni_SiPM/'
    ASCII_inputfile = outfolder + 'ASCII/' + runfile + '.compact.gz'
    ASCII_outputfile = outfolder + 'GZIP/' + runfile + '.gz'
    GZIP_inputfile = ASCII_outputfile
    GZIP_tempoutdir = '/eos/user/p/pmontigu/Functions/ASCIItoNPZ/Tempfiles/'
    
    # ==================================================================================================
    # FIRST PROCESS CALL
    # ==================================================================================================
    # Prepare for the execution of the first script (ASCII to GZIP converter).
    period = '7'    # Periodicity of the ASCII lines
    ind_in = '3'    # Remove the first [ind_in] values from each line loaded
    ind_out = '-3'  # Remove the last [-ind_out] values from each line loaded

    # Call the script
    print(f"ASCII to GZIP file conversion running...")
    start_time = time.perf_counter()
    if (runfile + '.gz') not in os.listdir(outfolder + 'GZIP/'):
        subprocess.call(['python3', ASCII_processfile, ASCII_inputfile, ASCII_outputfile, period, ind_in, ind_out])
        print(f"Conversion completed.")
    else:
        print(f"File already existing. Conversion skipped.")
    
    curr_time = time.perf_counter()
    eltime = curr_time - start_time
    print(f"Elapsed time: {eltime:.4} s")
    
    # ==================================================================================================
    # END OF FIRST PROCESS CALL; BEGINNING OF SECOND PROCESS CALL
    # ==================================================================================================
    # Define the columns' names
    c0 = ['xRaw_' + str(i) for i in range(4)]
    c1 = ['digiPh_' + str(i) for i in range(6)]
    c2 = ['digiTime_' + str(i) for i in range(6)]
    c3 = ['time_run']
    c4 = ['time_abs']
    c5 = ['eventNumber']
    c6 = ['wave0_' + str(i) for i in range(1024)]
    c7 = ['wave1_' + str(i) for i in range(1024)]
    c8 = ['wave2_' + str(i) for i in range(1024)]
    c9 = ['wave3_' + str(i) for i in range(1024)]
    c10 = ['wave4_' + str(i) for i in range(1024)]
    c11 = ['wave5_' + str(i) for i in range(1024)]
    nvars = 12 # Number of variables indexed as cN
    
    colnames = []
    for i in range(nvars):
        vec = eval('c'+str(i))
        colnames += vec
        
    # Define two dictionaries that associate to every variable name a datatype and a length
    col_dtypes = {"xRaw": "float64",
                    "digiPh": "float64" ,
                    "digiTime": "float64",
                    "time_run": "int64",
                    "time_abs": "int64",
                    "eventNumber": "int64",
                    "wave0": "float64",
                    "wave1": "float64",
                    "wave2": "float64",
                    "wave3": "float64",
                    "wave4": "float64",
                    "wave5": "float64"}
    col_inds = {"xRaw": np.arange(0,4),
                    "digiPh": np.arange(4,10),
                    "digiTime": np.arange(10,16),
                    "time_run": np.array([16]),
                    "time_abs": np.array([17]),
                    "eventNumber": np.array([18]),
                    "wave0": np.arange(19,1043),
                    "wave1": np.arange(1043,2067),
                    "wave2": np.arange(2067,3091),
                    "wave3": np.arange(3091,4115),
                    "wave4": np.arange(4115,5139),
                    "wave5": np.arange(5139,6163)}
    
    # Save the dictionaries and list to pickle files
    listfile = GZIP_tempoutdir + 'colnames.pkl'
    with open(listfile, 'wb') as f:
        pickle.dump(colnames, f)
        
    dictfile1 = GZIP_tempoutdir + 'col_dtypes.pkl'
    with open(dictfile1, 'wb') as f:
        pickle.dump(col_dtypes, f)
        
    dictfile2 = GZIP_tempoutdir + 'col_inds.pkl'
    with open(dictfile2, 'wb') as f:
        pickle.dump(col_inds, f)
    
    # Iterate over every dictionary key (i.e., column name). For each one, call the script that will load the 
    # corresponding file and save it.
    print(f"Extracting EOS/GZIP files...")
    curr_time = time.perf_counter()
    
    for i,col in enumerate(col_inds.keys()):
        print(f"Iteration n. {i+1} out of {len(col_inds.keys())}")
        # If the file already exists, skip the creation phase.
        tempextension = runfile + col + '.npz' # Something like 'run500898wave7.npz'
        tempoutputfile = GZIP_tempoutdir + tempextension # Actually the file name!
        
        if tempextension not in os.listdir(GZIP_tempoutdir):
            subprocess.call(['python3', GZIP_processfile, GZIP_inputfile, tempoutputfile, listfile, dictfile1, dictfile2, col])
        else:
            print(f"File already existing. Iteration skipped.")
        
        eltime = time.perf_counter()-curr_time
        curr_time = time.perf_counter()
        print(f"Iteration time: {eltime:.4} s")
        print("")
        print("")
        
    print(f"Extraction completed.")
    
    # Now load all the files into the workspace, as elements of a new dictionary
    print(f"Loading the files needed for the creation of EOS/NPZ file...")
    savedic = {}
    for i,col in enumerate(col_inds.keys()):
        tempextension = runfile + col + '.npz' # Something like 'run500898wave7.npz'
        tempoutputfile = GZIP_tempoutdir + tempextension # Actually the file name!
        tempvar = np.load(tempoutputfile)
        savedic[col] = tempvar
        
    # Now save the arrays in a new file
    print(f"Creation of EOS/NPZ file...")
    if len(sys.argv) > 2: # That is, if the argument n.2 does not exists, set ifwave to True
        ifwave = sys.argv[2]
    else:
        ifwave = True
        
    curr_time = time.perf_counter()
    if ifwave:
        GZIP_outputfile = outfolder + 'NPZ/' + runfile + '.npz'
        np.savez_compressed(GZIP_outputfile,
                            xRaw = savedic["xRaw"]['arr'],
                            digiPh = savedic["digiPh"]['arr'],
                            digiTime = savedic["digiTime"]['arr'],
                            time_run = savedic["time_run"]['arr'],
                            time_abs = savedic["time_abs"]['arr'],
                            eventNumber = savedic["eventNumber"]['arr'],
                            wave0 = savedic["wave0"]['arr'],
                            wave1 = savedic["wave1"]['arr'],
                            wave2 = savedic["wave2"]['arr'],
                            wave3 = savedic["wave3"]['arr'],
                            wave4 = savedic["wave4"]['arr'],
                            wave5 = savedic["wave5"]['arr'])
    else:
        GZIP_outputfile = outfolder + 'NPZ/' + runfile + 'nowave.npz'
        np.savez_compressed(GZIP_outputfile,
                            xRaw = savedic["xRaw"]['arr'],
                            digiPh = savedic["digiPh"]['arr'],
                            digiTime = savedic["digiTime"]['arr'],
                            time_run = savedic["time_run"]['arr'],
                            time_abs = savedic["time_abs"]['arr'],
                            eventNumber = savedic["eventNumber"]['arr'])
    print(f"NPZ file created.")
    eltime = np.round((time.perf_counter() - curr_time)/60) # from s to min
    print(f"Elapsed time: {eltime:.4} min")
    tottime = np.round((time.perf_counter() - start_time)/60) # from s to min
    print(f"Total elapsed time: {tottime:.4} min")
    
    # ==================================================================================================
    # END OF SECOND PROCESS CALL
    # ==================================================================================================