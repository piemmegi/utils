'''
----------------------------------------------------------------------------------------
                            ASCII to GZIP CONVERTER SCRIPT
----------------------------------------------------------------------------------------
The aim of this script is to load an input-given ASCII file, composed by groups of lines
written with a fixed periodicity (such as 1 line of input data and then N lines of
waveforms) and linearize it in a table-formatted GZIP file.

LIST OF INPUTS:
1. File to be loaded (ASCII, from EOS)
2. File to be written (GZIP, in EOS)
3. Periodicity of the file (i.e., total number of lines per group)
4. Indexes (from the beginning) to be eliminated from the strings loaded
    For example, if in the loading operation of a byte-sequence it appears a beginning string
    such as "b' ", then this variable should be set to 3
5. Indexes (from the end) to be eliminated from the strings loaded
    For example, if in the loading operation of a bute-sequence it appears an ending string
    such as "\n'", then this variable should be set to 3
'''

# First of all, import the required modules
import gzip, sys

# Define the hard-coded variable (as imported from the input arguments)
readfilename = sys.argv[1]
writefilename = sys.argv[2]
period = int(sys.argv[3])
ind_in = int(sys.argv[4])
ind_out = int(sys.argv[5])
checkline = 50000 # Every 10k events, print something

# Open the to-be-read file and the to-be-written file
with gzip.open(readfilename) as rf:
    with gzip.open(writefilename, 'wb') as wf:
        # Iterate over every line available in the read file
        wline = ''
        k = 0       # Periodicity reference index
        l = 0       # General counting index
        for line in rf.readlines():
            # Load the first line. If necessary, remove excess characters.
            if l % checkline == 0:
                print(f"Reading line n. {l}")
            
            templine = str(line)[ind_in:ind_out]
            wline += templine.replace('  ',' ')  # OPTIONAL: remove the double spaces, if present
            wline += ' '                         # Add a terminal white space, to separate different lines
            k += 1
            l += 1
            
            # If k has reached the periodicity index, write the line and reset the counter and the line variable
            if k == period:
                wline = wline[:-1]             # Remove a trailing white space
                wline += '\n'                  # Add the line terminator
                wf.write(bytes(wline,'utf8'))  # Write the line
                k = 0                          # Reset the periodicity counter
                
                # Re-create the empty line
                wline = ''
                
# End of the script

