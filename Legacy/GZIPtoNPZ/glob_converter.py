# ----------------------------------------------------------------------------------------
#                                 GZIP to NPZ CONVERTER SCRIPT
# ----------------------------------------------------------------------------------------
# The aim of this script is to take a huge GZIP file as input (like the ones produced in
# a test beam run) and convert it to a NPZ file. This conversion is made entirely in
# Python. The idea is the following: there is a second script, called corescript.py,
# which is called multiple times. Each time, it load a single column from the GZIP file
# (or a group of columns) and converts it to a predefined format. Then, it saves it in a
# NPZ file as a single array. Finally, the globconverter loads all the NPZ files with
# the memmap mode, as to not overflow the memory available, then it saves them in a unique
# NPZ file.

# ----------------------------------------------------------------------------------------
#                                 INSTRUCTIONS FOR USAGE
# ----------------------------------------------------------------------------------------
# In order to execute this script, simply put it in a folder (where there should be, also,
# the loc_converter.py file and a subfolder called "Tempvariables"). Adjust addresses across
# the script to your needs. Then, open a terminal and simply run the command:
#
# python3 /eos/user/p/pmontigu/Functions/GZIPtoNPZ/glob_converter.py [run number] [ifwave = True]
#
# Variable ifwave is, hence, supposed to be True unless stated otherwise

# ----------------------------------------------------------------------------------------
#                                        CODE 
# ----------------------------------------------------------------------------------------
# First of all, import the required modules
import os, glob, subprocess, pickle, time, sys
import numpy as np

# Define the relevant variables for the execution of the iteration: input file, output directories
start_time = time.perf_counter()
runfile = str(sys.argv[1])
inputfile = '/eos/user/v/vmascagn/STORM_2021/testbeam_data/GZIP/run' + runfile + '.gz'
tempoutputdir = '/eos/user/p/pmontigu/Functions/GZIPtoNPZ/Tempfiles/'

# Define the columns' names
c0 = ['xRaw_' + str(i) for i in range(8)]
c1 = ['nStripHit_' + str(i) for i in range(8)]
c2 = ['nHit_' + str(i) for i in range(8)]
c3 = ['digiBase_' + str(i) for i in range(32)]
c4 = ['digiPh_' + str(i) for i in range(32)]
c5 = ['digiTime_' + str(i) for i in range(32)]
c6 = ['gonio_' + str(i) for i in range(5)]
c7 = ['spill']
c8 = ['step']
c9 = ['eventNumber']
c10 = ['wave0_' + str(i) for i in range(1031)]
c11 = ['wave1_' + str(i) for i in range(1031)]
c12 = ['wave2_' + str(i) for i in range(1031)]
c13 = ['wave3_' + str(i) for i in range(1031)]
c14 = ['wave4_' + str(i) for i in range(1031)]
c15 = ['wave5_' + str(i) for i in range(1031)]
c16 = ['wave6_' + str(i) for i in range(1031)]
c17 = ['wave7_' + str(i) for i in range(1031)]

colnames = []
for i in range(18):
    vec = eval('c'+str(i))
    colnames += vec
    
# Define two dictionaries that associate to every variable name a datatype and a length
col_dtypes = {"xRaw": "float32", 
                "nStripHit": "uint16",
                "nHit": "uint16",
                "digiBase": "uint16",
                "digiPh": "uint16" ,
                "digiTime": "uint16",
                "gonio": "float32",
                "spill": "int32",
                "step": "int32",
                "eventNumber": "int32",
                "wave0": "float64", #"uint16",
                "wave1": "float64", #"uint16",
                "wave2": "float64", #"uint16",
                "wave3": "float64", #"uint16",
                "wave4": "float64", #"uint16",
                "wave5": "float64", #"uint16",
                "wave6": "float64", #"uint16",
                "wave7": "float64"} #"uint16"}
col_inds = {"xRaw": np.arange(0,8), 
                "nStripHit": np.arange(8,16),
                "nHit": np.arange(16,24),
                "digiBase": np.arange(24,56),
                "digiPh": np.arange(56,88),
                "digiTime": np.arange(88,120),
                "gonio": np.arange(120,125),
                "spill": np.array([125]),
                "step": np.array([126]),
                "eventNumber": np.array([127]),
                "wave0": np.arange(133,1157),
                "wave1": np.arange(1164,2188),
                "wave2": np.arange(2195,3219),
                "wave3": np.arange(3226,4250),
                "wave4": np.arange(4257,5281),
                "wave5": np.arange(5288,6312),
                "wave6": np.arange(6319,7343),
                "wave7": np.arange(7350,8374)}

# Save the dictionaries and list to pickle files
listfile = '/eos/user/p/pmontigu/Functions/GZIPtoNPZ/colnames.pkl'
with open(listfile, 'wb') as f:
    pickle.dump(colnames, f)
    
dictfile1 = '/eos/user/p/pmontigu/Functions/GZIPtoNPZ/col_dtypes.pkl'
with open(dictfile1, 'wb') as f:
    pickle.dump(col_dtypes, f)
    
dictfile2 = '/eos/user/p/pmontigu/Functions/GZIPtoNPZ/col_inds.pkl'
with open(dictfile2, 'wb') as f:
    pickle.dump(col_inds, f)

    
# Iterate over every dictionary key (i.e., column name). For each one, call the script that will load the 
# corresponding file and save it.
print(f"Extracting EOS/GZIP files...")
processfile = '/eos/user/p/pmontigu/Functions/GZIPtoNPZ/loc_converter.py'
for i,col in enumerate(col_inds.keys()):
    print(f"Iteration n. {i+1} out of {len(col_inds.keys())}")
    # If the file already exists, skip the creation phase.
    tempextension = 'run' + runfile + col + '.npz' # Something like 'run500898wave7.npz'
    tempoutputfile = tempoutputdir + tempextension # Actually the file name!
    
    if tempextension not in os.listdir(tempoutputdir):
        subprocess.call(['python3', processfile, inputfile, tempoutputfile, listfile, dictfile1, dictfile2, col])
    else:
        print(f"File already existing. Iteration skipped.")
    
    if i == 0:
        curr_time = time.perf_counter()
        eltime = curr_time-start_time
        print(f"Iteration time: {eltime:.4} s")
    else:
        eltime = time.perf_counter()-curr_time
        curr_time = time.perf_counter()
        print(f"Iteration time: {eltime:.4} s")
    print("")
    print("")
    
print(f"Extraction completed.")

# Now load all the files into the workspace, as elements of a new dictionary
print(f"Loading the files needed for the creation of EOS/NPZ file...")
savedic = {}
for i,key in enumerate(col_inds.keys()):
    tempoutputfile = tempoutputdir + 'run' + runfile + key + '.npz'
    tempvar = np.load(tempoutputfile)
    savedic[key] = tempvar
    
# Now save the arrays in a new file
print(f"Creation of EOS/NPZ file...")
if len(sys.argv) > 2: # That is, if the argument n.2 does not exists, set ifwave to True
    ifwave = sys.argv[2]
else:
    ifwave = True
    
if ifwave:
    outputfile = '/eos/user/p/pmontigu/Datadirs/datadir_2021/KLEVER_NPZ/run' + runfile + '.npz'
    np.savez_compressed(outputfile, 
                        xRaw = savedic["xRaw"]['arr'],
                        nStripHit = savedic["nStripHit"]['arr'],
                        nHit = savedic["nHit"]['arr'],
                        digiBase = savedic["digiBase"]['arr'],
                        digiPh = savedic["digiPh"]['arr'],
                        digiTime = savedic["digiTime"]['arr'],
                        gonio = savedic["gonio"]['arr'],
                        spill = savedic["spill"]['arr'],
                        step = savedic["step"]['arr'],
                        eventNumber = savedic["eventNumber"]['arr'],
                        wave0 = savedic["wave0"]['arr'],
                        wave1 = savedic["wave1"]['arr'],
                        wave2 = savedic["wave2"]['arr'],
                        wave3 = savedic["wave3"]['arr'],
                        wave4 = savedic["wave4"]['arr'],
                        wave5 = savedic["wave5"]['arr'],
                        wave6 = savedic["wave6"]['arr'],
                        wave7 = savedic["wave7"]['arr'])
else:
    outputfile = '/eos/user/p/pmontigu/Datadirs/datadir_2021/KLEVER_NPZ/run' + runfile + 'nowave.npz'
    np.savez_compressed(outputfile, 
                        xRaw = savedic["xRaw"]['arr'],
                        nStripHit = savedic["nStripHit"]['arr'],
                        nHit = savedic["nHit"]['arr'],
                        digiBase = savedic["digiBase"]['arr'],
                        digiPh = savedic["digiPh"]['arr'],
                        digiTime = savedic["digiTime"]['arr'],
                        gonio = savedic["gonio"]['arr'],
                        spill = savedic["spill"]['arr'],
                        step = savedic["step"]['arr'],
                        eventNumber = savedic["eventNumber"]['arr'])
print(f"NPZ file created.")
eltime = np.round((time.perf_counter() - start_time)/60) # from s to min
print(f"Elapsed time: {eltime:.4} min")

# Remember to clear the temporary file folder!