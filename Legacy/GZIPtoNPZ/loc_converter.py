# ----------------------------------------------------------------------------------------
#                                 GZIP to NPZ CONVERTER SCRIPT
# ----------------------------------------------------------------------------------------
# The aim of this script is to load an input-given GZIP file, extract a column from it
# (or a group of columns) with given datatype, and then save it in a npz file.
# LIST OF INPUTS:
# 1. File to be loaded (GZIP, from EOS)
# 2. File to be written (NPZ, in EOS)
# 3. List to be used in the execution (column names)
# 4. Dictionary to be used in the execution (columns/dtypes)
# 5. Dictionary to be used in the execution (columns/indexes)
# 6. Dictionary key of the columns to be loaded

# First of all, import the required modules
import sys, pickle
import numpy as np
import pandas as pd

# Load the columns' names
with open(sys.argv[3], 'rb') as f:
    colnames = pickle.load(f)

# Load the two dictionaries containing column datatypes and indexes
with open(sys.argv[4], 'rb') as f:
    col_dtypes = pickle.load(f)
    
with open(sys.argv[5], 'rb') as f:
    col_inds = pickle.load(f)

# Define the column to be used for the upload and its datatype
loadcolinds = col_inds[sys.argv[6]]
loadcoldtype = col_dtypes[sys.argv[6]]

# Load the input file with pandas
input_file = sys.argv[1]
data = pd.read_csv(input_file, sep = ' ', header = 0, compression = 'gzip', error_bad_lines = False, memory_map = True, names = colnames, index_col = False, skiprows = 0, usecols = loadcolinds, dtype = loadcoldtype)


# Save the extracted column
np.savez_compressed(sys.argv[2], arr = data)