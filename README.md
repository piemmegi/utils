# utils

Functions and scripts useful for common analysis performed in `Python`.

Toolkit developed by [piemmegi](mailto:monti.guarnieri.p@gmail.com), University of Trieste.

Last update of this guide: 2024/06/03